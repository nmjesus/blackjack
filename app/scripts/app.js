(function() {
  'use strict';

  var MAX_PLAYERS = 5;
  var game;

  var dealerCardsPlaceholder = document.querySelector('.dealer .cards-placeholder');
  var settingsEl = document.getElementById('settings');

  var clickActions = {
    'hit': _call_player_action.bind(this, 'hit'),
    'double': _call_player_action.bind(this, 'double'),
    'deal': _call_player_action.bind(this, 'deal'),
    'stand': _call_player_action.bind(this, 'stand')
  };

  function init() {
    setEvents();
  }

  function setEvents() {
    settingsEl.addEventListener('submit', startHandler, false);
    document.body.addEventListener('click', clickHandler, false);
  }

  function compileTemplate(players) {
    var listPlayers = document.getElementById('list-players');
    var listPlayersContainer = document.getElementById('list-players-container');
    var t = Handlebars.compile(listPlayers.innerHTML);
    listPlayersContainer.innerHTML = (t({ players: players }));
  }

  function startHandler(ev) {
    var i, players = [], n = Number.parseInt(this.nPlayers.value, 10);
    n = n > MAX_PLAYERS ? MAX_PLAYERS : n;
    for(i = 1; i<=n; i++) {
      players.push(['Player', i].join(' '));
    }
    game = new Blackjack.Game(players);
    game.players[0].deal(0, game.deck, cardsDrewCB.bind(null, dealerCardsPlaceholder));
    window.blackjackApp = game;
    setBoard();
    compileTemplate(game.players);
    ev.preventDefault();
  }

  function setBoard() {
    document.body.classList.remove('settings');
    document.body.classList.add('playing');
  }

  function clickHandler(ev) {
    var target = ev.target;
    var action;
    while(target !== document) {
      action = target.getAttribute('data-click-action');
      if(action) {
        return clickActions[action](ev);
      }
      target = target.parentNode;
    }
  }

  function getPlayerEl(target) {
    var isPlayer = false;
    while(target !== document) {
      isPlayer = target.hasAttribute('data-player');
      if(isPlayer) {
        return target;
      }
      target = target.parentNode;
    }
    return false;
  }

  function getPlayerId(target) {
    return getPlayerEl(target).getAttribute('data-player-id');
  }

  function getBet(target) {
    return getPlayerEl(target).querySelector('input[type="number"].bet').value;
  }

  function generateCard(target, card, idx) {
    var cardsPlaceholder = getPlayerEl(target).querySelector('.cards-placeholder');
    var div = document.createElement('div');
    var p = game.players[getPlayerId(target)];
    var showBack = p.isDealer && idx === 0 && p.hand.length === 2;
    if(showBack) {
      div.className = ['card', 'deck', 'back'].join(' ');
    } else {
      div.className = ['card', card.suit, card.card.name].join(' ');
    }
    div.style.left = (cardsPlaceholder.children.length * 15) + 'px';
    cardsPlaceholder.appendChild(div);
  }

  function cardsDrewCB(target, cards) {
    var id = getPlayerId(target);
    cards.forEach(generateCard.bind(null, target));
    getPlayerEl(target).classList.add('playing');
    game.players[id].state = 'playing';
    if(game.players[id].totalHand() > 21) {
      playerStandCB(target, true);
    }
  }

  function playerStandCB(target, lost) {
    var id = getPlayerId(target);
    target =  getPlayerEl(target);
    target.classList.remove('playing');
    target.classList.add('waiting');
    
    game.players[id].state = 'standing';
    if(lost) {
      game.players[id].state = 'busted';
      target.classList.add('bust');
    }
  }

  function _call_player_action(which, ev) {
    var fn, args;
    var target = ev.target;
    var playerId = getPlayerId(target);
    var p = game.players[playerId];
    switch(which) {
    case 'hit':
      fn = 'hit';
      args = [game.deck, cardsDrewCB.bind(null, target)];
      break;
    case 'double':
      fn = 'double';
      args = [game.deck, cardsDrewCB.bind(null, target)];
      break;
    case 'deal':
      fn = 'deal';
      args = [getBet(target), game.deck, cardsDrewCB.bind(null, target)];
      break;
    case 'stand':
      fn = 'stand';
      args = [playerStandCB.bind(null, target)];
      break;
    }

    p[fn].apply(p, args);

    return playDealer();
  }

  function playDealer() {
    var id, p, cardEl;
    if(game.playersActive() === 1) {
      cardEl = document.querySelector('.dealer .card.deck.back, .dealer[data-player] .card');
      id = getPlayerId(cardEl);
      turnCard(id, cardEl);
      p = game.players[id];
      p.hit(game.deck, cardsDrewCB.bind(null, cardEl));

      // TODO: after dealer win, set the victory
      if(game.shouldPlay()) {
        setTimeout(playDealer, 1000);
      } else {
        playerStandCB(cardEl, false);
      }
    }
  }

  function turnCard(id, cardEl) {
    var p = game.players[id];
    var card = p.hand[0].card;
    cardEl.classList.remove('deck', 'back');
    cardEl.classList.add(card.suit, card.name);
    return card;
  }

  init();
})();
