(function() {
  var cards = [
    { value: 1, name: 'ace' },
    { value: 2, name: 'two' },
    { value: 3, name: 'three' },
    { value: 4, name: 'four' },
    { value: 5, name: 'five' },
    { value: 6, name: 'six' },
    { value: 7, name: 'seven' },
    { value: 8, name: 'eight' },
    { value: 9, name: 'nine' },
    { value: 10, name: 'ten' },
    { value: 'J', name: 'jack' },
    { value: 'Q', name: 'queen' },
    { value: 'K', name: 'king'}
  ];
  var suits = ['clubs', 'diamonds', 'hearts', 'spades'];

  var Deck = {
    create: function() {
      var deck = [];
      suits.forEach(function(suit) {
        cards.forEach(function(card) {
          deck.push({
            card: card,
            suit: suit,
            value: (typeof card.value === 'number' ? card.value : 10)
          });
        });
      });
      return deck;
    }
  };

  window.Blackjack.Deck = Deck;
})();
