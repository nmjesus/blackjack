(function() {
  'use strict';

  function Player(name, money, isDealer) {
    this.hand = [];
    this.name = name;
    this.state = 'waiting';
    this.money = money;
    this.isDealer = !!isDealer;
    this.bet = 0;
  }

  Player.prototype.hit = function(deck, cb) {
    var card = deck.pop();
    this.hand.push(card);
    Blackjack.Utils.safeCall(cb, [[].concat(card)]);
    return this.hand;
  };

  Player.prototype.deal = function(bet, deck, cb) {
    var cards = [deck.pop(), deck.pop()];
    cards.forEach(function(card) {
      this.hand.push(card);
    }, this);
    this.setBet(parseInt(bet, 10));
    this.updateMoney(this.bet * -1);
    Blackjack.Utils.safeCall(cb, [cards]);
    return this.hand;
  };

  Player.prototype.double = function(deck, cb) {
    var card = deck.pop();
    this.updateMoney(this.bet * -1);
    this.setBet(this.bet);
    this.hand.push(card);
    Blackjack.Utils.safeCall(cb, [[].concat(card)]);
    return this.hand;
  };

  Player.prototype.stand = function(cb) {
    Blackjack.Utils.safeCall(cb);
  };

  Player.prototype.setBet = function(bet) {
    this.bet += bet;
  };

  Player.prototype.updateMoney = function(amount) {
    return (this.money += amount);
  };

  Player.prototype.totalHand = function() {
    var total = 0, aceFound = 0;
    this.hand.forEach(function(c) {
      aceFound += (c.value === 1 ? 1 : 0);
      total += (c.value === 1 ? 11 : c.value);
    });
    while(total > 21 && aceFound > 0) {
      total -= 10;
      aceFound--;
    }
    return total;
  };

  Player.prototype.clear = function() {
    this.hand = [];
  }

  window.Blackjack.Player = Player;
})();
