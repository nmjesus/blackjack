(function() {
  var Utils = {
    shuffle: function(arr) {
      return arr.sort(function() { return 0.5 - Math.random(); });
    },

    safeCall: function(fn, args, ctx) {
      try {
        if(fn && typeof fn === 'function') {
          fn.apply(ctx || null, args)
        }
      } catch(e) {
        console.error('Error calling', fn, 'with args', args, 'and context', ctx, e.message);
      }
    }
  }

  window.Blackjack.Utils = Utils;
})();
