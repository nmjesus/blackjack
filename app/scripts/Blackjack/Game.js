(function() {
  'use strict';

  var START_MONEY = 1000;

  function createPlayers(players) {
    var res = [];
    res.push(new Blackjack.Player('Dealer', +Infinity, true));
    players.forEach(function(player) {
      res.push(new Blackjack.Player(player, START_MONEY));
    });
    return res;
  }

  function Game(players) {
    this.deck = Blackjack.Utils.shuffle(Blackjack.Deck.create());
    this.players = createPlayers(players);
  }

  Game.prototype.setState = function(p, state) {
    state = state || '';

    if(p.totalHand() > 21) {
      state = 'bust';
    }
    if(p.totalHand() === 21) {
      state = 'blackjack';
    }
    
    p.state = state;
    return state;
  }

  Game.prototype.playersActive = function() {
    var playersActive = 0;
    this.players.forEach(function(p) {
      if(p.state === 'playing' || p.state === 'waiting') {
        playersActive++;
      }
    });
    return playersActive;
  }

  Game.prototype.whoWon = function() {
    var biggerValue = 0;
    var winner;
    this.players.filter(function(p) { return p.state !== 'busted'; }).forEach(function(p) {
      if(p.totalHand() > biggerValue) {
        winner = p;
      }
    });
    return winner;
  }

  Game.prototype.shouldPlay = function() {
    if(this.players[0].totalHand() > 15) {
      return Math.random() > 0.5;
    }
    return true;
  }

  window.Blackjack.Game = Game;
})();
