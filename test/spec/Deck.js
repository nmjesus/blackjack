(function () {
  'use strict';

  describe('Deck suite', function () {
    var deck;

    beforeEach(function() {
      deck = Blackjack.Deck.create();
    });
    it('Should create a deck of 52 cards', function () {
      expect(deck.length).to.be.equal(52);
    });

    it('First card should be a ace of clubs', function() {
      expect(deck[0].suit).to.be.equal('clubs');
      expect(deck[0].card.value).to.be.equal(1);
    });

    it('Last card should be a king of spades', function() {
      var last = deck.pop()
      expect(last.suit).to.be.equal('spades');
      expect(last.card.value).to.be.equal('K');
    });

    it('Should have THE ACE OF SPADES, THE ACE OF SPADES', function() {
      var aceOfSpades = deck[39];
      expect(aceOfSpades.suit).to.be.equal('spades');
      expect(aceOfSpades.card.value).to.be.equal(1);
    });


    it('Should have the correct card values', function() {
      var king = deck.pop();
      var queen = deck.pop();
      var jack = deck.pop();
      var ten = deck.pop();
      var nine = deck.pop();
      expect(king.value).to.be.equal(10);
      expect(queen.value).to.be.equal(10);
      expect(jack.value).to.be.equal(10);
      expect(ten.value).to.be.equal(10);
      expect(nine.value).to.be.equal(9);
      expect(deck[0].value).to.be.equal(1);
      expect(deck[1].value).to.be.equal(2);
    });
  });
})();
