(function() {
  'use strict';

  describe('Game suit', function() {
    var g, deckLen = 52;

    beforeEach(function() {
      g = new Blackjack.Game(['foo', 'bar']);
    });

    it('Should init Game', function() {
      expect(g).to.be.an.instanceof(Blackjack.Game);
    });

    it('Should have deck', function() {
      expect(g.deck).to.be.an('array');
      expect(g.deck.length).to.be.equal(deckLen);
    });

    it('Should have 3 players', function() {
      expect(g.players.length).to.be.equal(3);
    });

    it('Should have 1 dealer', function() {
      expect(g.players[0].isDealer).to.be.equal(true);
    });

    it('Should have 3 active players', function() {
      g.players.forEach(function(p) {
        p.deal(100, g.deck, function() {
          p.state = 'playing';
        });
      });
      expect(g.playersActive()).to.be.equal(3);
    });
  });
})();
