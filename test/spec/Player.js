(function() {
  describe('Player suite', function() {
    var deckLen = 52;
    var p, g;
    beforeEach(function() {
      p = new Blackjack.Player('Jesus', 100, true);
      g = new Blackjack.Game(['foo', 'bar']);
    });

    it('Should create a player', function() {
      expect(p.hand).to.be.a('array');
      expect(p.name).to.be.a('string');
      expect(p.money).to.be.a('number');
      expect(p.isDealer).to.be.a('boolean');
    });

    it('Should update money', function() {
      expect(p.money).to.be.equal(100);
      p.updateMoney(-50);
      expect(p.money).to.be.equal(50);
      p.updateMoney(25);
      expect(p.money).to.be.equal(75);
    });

    it('Should update money on deal or double', function() {
      p.deal(10, g.deck);
      expect(p.money).to.be.equal(90);
      expect(p.bet).to.be.equal(10);

      p.double(g.deck);
      expect(p.bet).to.be.equal(20);
      expect(p.money).to.be.equal(100-p.bet);
    });

    it('Should give two cards when deal', function() {
      var cards = 0;
      g.players.forEach(function(p) {
        expect(p.deal(100, g.deck).length).to.be.equal(2);
        cards += 2;
      });

      expect(g.deck.length).to.be.equal(deckLen-cards);
    });

    it('Should set bet', function() {
      p.deal(100, g.deck);
      expect(p.bet).to.be.equal(100);
    });

    it('Should add a card into hands player on hit', function() {
      p.deal(100, g.deck);
      p.hit(g.deck);
      expect(p.hand.length).to.be.equal(3);
    });

    it('Should add a card into hands player on double', function() {
      p.deal(100, g.deck);
      p.double(g.deck);
      expect(p.hand.length).to.be.equal(3);
    });

    it('Should count figures value', function() {
      p.hand.push({
        card: { value: 'Q', name: 'queen' },
        suit: 'clubs',
        value: 10
      });
      expect(p.totalHand()).to.be.equal(10);
    });

    it('Should count hand value', function() {
      p.deal(100, g.deck);
      expect(p.totalHand()).to.be.greaterThan(0);
      expect(p.totalHand()).to.be.lessThan(22);
    });

    it('Should handle ace value properly', function() {
      p.hand.push({
        card: { value: 1, name: 'ace'},
        suit: 'clubs',
        value: 1
      });
      expect(p.totalHand()).to.be.equal(11);
      p.hand.push({
        card: { value: 1, name: 'ace'},
        suit: 'diamonds',
        value: 1
      });
      expect(p.totalHand()).to.be.equal(12);
      p.hand.push({
        card: { value: 10, name: 'ten'},
        suit: 'diamonds',
        value: 10
      });
      expect(p.totalHand()).to.be.equal(12);
      p.hand.push({
        card: { value: 10, name: 'ten'},
        suit: 'clubs',
        value: 10
      });
      expect(p.totalHand()).to.be.equal(22);
    });
  });
})();
